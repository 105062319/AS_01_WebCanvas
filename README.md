# Software Studio 2018 Spring Assignment 01 Web Canvas

## Web Canvas
<img src="example01.gif" width="700px" height="500px"></img>

## Todo
1. **Fork the repo ,remove fork relationship and change project visibility to public.**
2. Create your own web page with HTML5 canvas element where we can draw somethings.
3. Beautify appearance (CSS).
4. Design user interaction widgets and control tools for custom setting or editing (JavaScript).
5. **Commit to "your" project repository and deploy to Gitlab page.**
6. **Describing the functions of your canvas in REABME.md**

## Scoring (Check detailed requirments via iLMS)

|                       **Item**                   | **Score** |
|:--------------------------------------------:|:-----:|
|               Basic components               |  60%  |
|                 Advance tools                |  35%  |
|            Appearance (subjective)           |   5%  |
| Other useful widgets (**describe on README.md**) | 1~10% |

## Reminder
* Do not make any change to our root project repository.
* Deploy your web page to Gitlab page, and ensure it works correctly.
    * **Your main page should be named as ```index.html```**
    * **URL should be : https://[studentID].gitlab.io/AS_01_WebCanvas**
* You should also upload all source code to iLMS.
    * .html or .htm, .css, .js, etc.
    * source files
* **Deadline: 2018/04/05 23:59 (commit time)**
    * Delay will get 0 point (no reason)
    * Copy will get 0 point
    * "屍體" and 404 is not allowed

***Canvas介紹

筆刷只有一種，但有四種顏色可以選擇，粗細由brush-size決定。並且使用一隻筆的游標。

當要使用橡皮擦時，點擊eraser左邊的按鈕，游標會變成銀白色的圓型，橡皮擦的大小也是由brush-size決定。

畫布左下方有Text input，可以輸入中文英文以及數字，但需要注意前一個使用的工具不可以是橡皮擦，否則會是白色，或是使用前先點擊顏色作為字體顏色。
Text input有兩種字體和兩種字型可以選擇。

當需要清空畫布時可以按下reset。

Rect可以劃出矩型，但畫完一個後要再次使用的話需再按一次，顏色也會被橡皮擦影響。
trian可以在四個角落畫出三角形作為裝飾，順序分別是右下，左下，右上，左上。可以先將brush-size調大做為基底，在一輪過後再調小，將三角形重疊作為裝飾。
此外，點擊trian時的游標為一個綁著頭巾的人臉。
circle可以在畫布偏下方的地方畫出圓圈，按許多次可以連成一排圓圈，可以向上面的三角形一樣疊加作為裝飾，或是純用單種顏色加上筆刷做造型。
點擊circle時的游標為一個戴著帽子的人臉。

Download即可下載圖片，背景為黑色，故使用橡皮擦時需要小心謹慎的使用。


